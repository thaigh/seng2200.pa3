﻿using System;
using System.Collections.Generic;
using Seng2200.Pa3.Storage;

namespace Seng2200.Pa3.Stages
{
    public class MasterStage
    {
        // Responsible for communicating to inbound and outbound queues

        public List<SubStage> Stages { get; set; } = new List<SubStage>();
        public InterStageStorage InboundQueue { get; private set; }
        public InterStageStorage OutboundQueue { get; private set; }

        public readonly string StageName;

        public event EventHandler<StartProcessingEventArgs> StartProcessingEvent;

        public MasterStage(string stageName, SubStage stage) :
            this (stageName, new List<SubStage> { stage }) { }

        public MasterStage(string stageName, List<SubStage> stages)
        {
            StageName = stageName;
            Stages = stages;

            foreach (var s in Stages)
            {
                s.StartProcessingItemEvent += StartProcessingSubstage;
                s.Parent = this;
            }
        }

        private void StartProcessingSubstage(object sender, StartProcessingEventArgs e)
        {
            StartProcessingEvent?.Invoke(sender, e);
        }

        public void AddStorage(InterStageStorage storageQueue, StorageType type)
        {
            switch (type)
            {
                case StorageType.Inbound:
                    {
                        InboundQueue = storageQueue;
                        foreach (var s in Stages) { s.InboundQueue = storageQueue; }
                        break;
                    }
                case StorageType.Outbound :
                    {
                        OutboundQueue = storageQueue;
                        foreach (var s in Stages) { s.OutboundQueue = storageQueue; }
                        break;
                    }
                default: throw new ArgumentException(string.Format("StorageType '{0}' is undefined", type), nameof(type));
            }
        }

        public bool HasAtLeastOneStageThatIsBlocked()
        {
            foreach (var s in Stages)
            {
                if (s.IsBlocked) return true;
            }
            return false;
        }

        public bool HasAtLeastOneStageThatIsStarved()
        {
            foreach (var s in Stages)
            {
                if (s.IsStarved) return true;
            }
            return false;
        }

        public bool HasAtLeastOneStageThatIsFinishedProcessing()
        {
            foreach (var s in Stages)
            {
                if (s.IsFinishedProcessing) return true;
            }
            return false;
        }

        public override string ToString()
        {
            return string.Format("MasterStage: {0}", StageName);
        }

    }
}
