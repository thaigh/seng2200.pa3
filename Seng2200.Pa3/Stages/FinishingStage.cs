﻿using System;
namespace Seng2200.Pa3.Stages
{
    public class FinishingStage : SubStage
    {
        public FinishingStage(double m, double n, double mult = 1) : base(m, n, mult)
        {
        }

        public override bool PassItemToOutboundQueue(double currentTime)
        {
            return base.PassItemToOutboundQueue(currentTime);
        }
    }
}
