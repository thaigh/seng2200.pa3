﻿namespace Seng2200.Pa3.Stages
{
    public enum ProcessingState
    {
        Processing,
        Blocked,
        Starved,
        FinishedProcessing,
        Empty,
        Ready
    }
}