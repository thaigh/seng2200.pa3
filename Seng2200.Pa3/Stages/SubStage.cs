﻿using System;
using System.Reflection.Metadata.Ecma335;
using Seng2200.Pa3.Items;
using Seng2200.Pa3.Storage;
using System.Reflection;

namespace Seng2200.Pa3.Stages
{
    public class SubStage : IProcessingStage
    {
        public ProcessingState State { get; private set; }

        public bool IsBlocked => State == ProcessingState.Blocked;
        public bool IsStarved => State == ProcessingState.Starved;
        public bool IsEmpty =>   State == ProcessingState.Empty;
        public bool IsReady => State == ProcessingState.Ready;
        public bool IsProcessing => State == ProcessingState.Processing;
        public bool IsFinishedProcessing =>   State == ProcessingState.FinishedProcessing;

        public readonly double Mean;
        public readonly double Range;
        public readonly double Multiplier;

        public string Name { get; set; }

        private static Random _rand = new Random();

        private IProcessingItem _item;

        public InterStageStorage InboundQueue { get; set; }
        public InterStageStorage OutboundQueue { get; set; }
        public MasterStage Parent { get; set; }

        public event EventHandler<StartProcessingEventArgs> StartProcessingItemEvent;
        public double ExpectedFinishTime { get; set; }

        public SubStage(double m, double n, double mult = 1)
        {
            Mean = m;
            Range = n;
            Multiplier = mult;
            State = ProcessingState.Starved;
        }

        public void FinishItem()
        {
            AssertState(ProcessingState.Processing);
            AssertItemState(shouldBeHoldingItem: true);

            State = ProcessingState.FinishedProcessing;
        }

        private void AssertState(ProcessingState expectedState) {
            if (State != expectedState)
                throw new Exception(
                    string.Format("State is not in expected state to call this function. Expected State {0}, but was in State {1}",
                                  expectedState, State));
        }

        private void AssertItemState(bool shouldBeHoldingItem) {
            if (shouldBeHoldingItem && _item == null)
                throw new Exception("Invalid state. There should be an item in this stage");

            if (!shouldBeHoldingItem && _item != null)
                throw new Exception("Invalid state. There should be no item in the stage");
        }

        public void StartProcessingItem()
        {
            AssertState(ProcessingState.Ready);
            AssertItemState(shouldBeHoldingItem: true);

            State = ProcessingState.Processing;
            double requiredProcessingTime = CalculateProcessingTime();

            StartProcessingItemEvent?.Invoke(this,
                new StartProcessingEventArgs { Stage = this, RequiredProcessingTime = requiredProcessingTime });
        }

        public void Block()
        {
            AssertState(ProcessingState.FinishedProcessing);
            State = ProcessingState.Blocked;
            Console.WriteLine("State: {0}: Blocking state", Name);
        }

        public void Unblock()
        {
            AssertState(ProcessingState.Blocked);
            State = ProcessingState.FinishedProcessing;
            Console.WriteLine("State: {0}: Unblocking state", Name);
        }

        public double CalculateProcessingTime()
        {
            return (Multiplier * Mean) + ((Multiplier * Range) * (_rand.NextDouble() - 0.5));
        }

        public void Starve()
        {
            AssertState(ProcessingState.Empty);
            State = ProcessingState.Starved;
            Console.WriteLine("State: {0}: Starving state", Name);
        }

        public void Unstarve()
        {
            AssertState(ProcessingState.Starved);
            State = ProcessingState.Empty;
            Console.WriteLine("State: {0}: Unstarving state", Name);
        }

        public virtual bool PassItemToOutboundQueue()
        {
            AssertState(ProcessingState.FinishedProcessing);
            AssertItemState(shouldBeHoldingItem: true);

            if (OutboundQueue.IsFull)
            {
                Block();
                return false;
            }

            IProcessingItem itm = RemoveItemFromStage();
            Console.WriteLine("Stage {0}: Passing item to Storage Queue {1}", Name, OutboundQueue.Name);
            OutboundQueue.Enqueue(itm);
            Console.WriteLine("Stage {0}: Stage is now empty", Name);

            return true;
        }

        private IProcessingItem RemoveItemFromStage()
        {
            IProcessingItem tmp = _item;
            _item = null;

            State = ProcessingState.Empty;
            Console.WriteLine("Stage {0}: Removing item from stage", Name);
            return tmp;
        }

        public virtual bool TakeItemFromInboundQueue()
        {
            AssertState(ProcessingState.Empty);
            AssertItemState(shouldBeHoldingItem: false);

            if (InboundQueue.IsEmpty)
            {
                Starve();
                return false;
            }

            Console.WriteLine("Stage {0}: Taking item from Storage Queue {1}", Name, InboundQueue.Name);

            State = ProcessingState.Ready; // HACK: Make this state ready, so that previous stage doesn't try to push item forward
            IProcessingItem itm = InboundQueue.Dequeue();
            AddItemToStage(itm);
            Console.WriteLine("Stage {0}: State is ready to start processing", Name);

            return true;
        }

        private void AddItemToStage(IProcessingItem itm)
        {
            _item = itm;
            //State = ProcessingState.Ready;
            Console.WriteLine("Stage {0}: Adding item to stage", Name);
        }

        public override string ToString()
        {
            return string.Format("SubStage: {0}, State: {1}]", Name, State);
        }

    }
}