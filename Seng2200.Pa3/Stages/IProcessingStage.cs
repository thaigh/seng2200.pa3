﻿namespace Seng2200.Pa3.Stages
{
    public interface IProcessingStage
    {
        void Block();
        void Starve();

        bool IsBlocked { get; }
        bool IsStarved { get; }
        bool IsEmpty { get; }

        double CalculateProcessingTime();
    }
}