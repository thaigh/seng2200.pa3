﻿﻿﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Seng2200.Pa3.Stages
{
    public class StartProcessingEventArgs : EventArgs
    {
        public SubStage Stage { get; set; }
        public double RequiredProcessingTime { get; set; }
    }

    public class SimulationEvent
    {
        public SubStage Stage { get; set; }
        public double FinishProcessingTime { get; set; }
    }

    public class EventComparer : IComparer<SimulationEvent>
    {
        public int Compare(SimulationEvent x, SimulationEvent y)
        {
            return (Math.Abs(x.FinishProcessingTime - y.FinishProcessingTime) < double.Epsilon)
                ? 0
                : (x.FinishProcessingTime < y.FinishProcessingTime)
                    ? -1
                    : 1;
        }
    }

}
