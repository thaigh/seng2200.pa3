﻿using System;
using Seng2200.Pa3.Items;
namespace Seng2200.Pa3.Stages
{
    public class BeginningStage : SubStage
    {
        public BeginningStage(double m, double n, double mult = 1) : base(m, n, mult)
        {
            base.State = ProcessingState.Processing;
        }

        public override bool TakeItemFromInboundQueue()
        {
            IProcessingItem itm = new Item();
            TakeItemFromInboundQueue(itm);
            return true;
        }
    }
}
