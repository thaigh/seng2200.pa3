﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Seng2200.Pa3
{
    public class DoubleComparer : IComparer<double>
    {
        public int Compare(double x, double y)
        {
            return (Math.Abs(x - y) < double.Epsilon)
                ? 0
                : (x < y)
                    ? -1
                    : 1;
        }
    }
}
