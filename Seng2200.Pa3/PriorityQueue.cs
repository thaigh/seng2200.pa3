﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace Seng2200.Pa3
{
    public class PriorityQueue<T>
    {
        private List<T> _queue = new List<T>();
        private IComparer<T> _compareFunction;

        public PriorityQueue(IComparer<T> comp)
        {
            _compareFunction = comp;
        }

        public void Enqueue(T item)
        {
            _queue.Add(item);
        }

        public T Dequeue()
        {
            T item = _queue.OrderBy(x => x, _compareFunction).FirstOrDefault();
            _queue.Remove(item);
            return item;
        }

        public bool IsEmpty
        {
            get { return _queue.Count < 1; }
        }
    }
}
