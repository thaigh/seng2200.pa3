﻿using System;
using System.Collections.Generic;
using Seng2200.Pa3.Items;
using Seng2200.Pa3.Stages;

namespace Seng2200.Pa3.Storage
{
    public class FinishedItemEventArgs : EventArgs
    {
        public Item FinishedItem { get; set; }
    }

    public class InfiniteOutboundStorage : InterStageStorage
    {
        public event EventHandler<FinishedItemEventArgs> FinishedItemProcessing;

        public List<IProcessingItem> FinishedItems { get; private set; }

        public InfiniteOutboundStorage(MasterStage prev) : base(1, null, prev)
        {
            FinishedItems = new List<IProcessingItem>();
        }

        public override bool IsFull { get { return false; } }
        public override bool IsEmpty { get { return FinishedItems.Count < 1; } }

        public override IProcessingItem Dequeue()
        {
            throw new Exception("Cannot dequeue items from Infinite Outbound Storage Queue");
        }

        public override void Enqueue(IProcessingItem item)
        {
            FinishedItems.Add(item);

            // HACK: Force convert IProcessingItem to be an Item
            FinishedItemProcessing?.Invoke(this, new FinishedItemEventArgs { FinishedItem = (Item)item });

            AttemptRestartOfPreviousStages();
        }
    }
}
