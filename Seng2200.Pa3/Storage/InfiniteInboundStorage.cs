﻿using System;
using Seng2200.Pa3.Stages;
using Seng2200.Pa3.Items;

namespace Seng2200.Pa3.Storage
{
    public class InfiniteInboundStorage : InterStageStorage
    {
        private ItemFactory _factory;

        public InfiniteInboundStorage(MasterStage next, ItemFactory factory) : base(1, next, null)
        {
            _factory = factory;
        }

        public override bool IsFull { get { return true; } }
        public override bool IsEmpty { get { return false; } }

        public override IProcessingItem Dequeue()
        {
            IProcessingItem itm = _factory.Create();
            return itm;
        }

        public override void Enqueue(IProcessingItem item)
        {
            throw new Exception("Cannot enqueue items into Infinite Inbound Storage Queue");
        }

    }
}
