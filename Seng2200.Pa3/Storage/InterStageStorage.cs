﻿using System;
using System.Collections.Generic;
using Seng2200.Pa3.Items;
using Seng2200.Pa3.Stages;
using System.Linq;

namespace Seng2200.Pa3.Storage
{
    public class InterStageStorage
    {
        private Queue<IProcessingItem> _queue;
        public readonly int MaxQueueSize;

        public virtual bool IsFull { get { return _queue.Count >= MaxQueueSize; } }
        public virtual bool IsEmpty { get { return _queue.Count <= 0; }}

        public MasterStage NextStage { get; private set; }
        public MasterStage PreviousStage { get; private set; }

        public string Name { get; set; }

        public InterStageStorage(int maxQueueSize, MasterStage next, MasterStage prev) {

            if (maxQueueSize < 1)
                throw new Exception("Max queue size must be greater than or equal to 1");

            MaxQueueSize = maxQueueSize;
            _queue = new Queue<IProcessingItem>(MaxQueueSize);

            NextStage = next;
            PreviousStage = prev;
        }

        public virtual void Enqueue(IProcessingItem item)
        {
            if (IsFull)
                throw new Exception("Interstage storage is full");

            _queue.Enqueue(item);

            AttemptRestartOfForwardStages();
        }

        public virtual IProcessingItem Dequeue()
        {
            if (IsEmpty)
                throw new Exception("Interstage storage is empty");

            IProcessingItem itm = _queue.Dequeue();

            AttemptRestartOfPreviousStages();

            return itm;
        }

        private void AttemptRestartOfForwardStages()
        {
            if (NextStage == null) return;

            foreach (var s in NextStage.Stages.OrderBy(s => s.ExpectedFinishTime))
            {
                if (s.IsStarved)
                    s.Unstarve();

                if (s.IsEmpty)
                    if (s.TakeItemFromInboundQueue())
                        s.StartProcessingItem();
            }
        }

        protected void AttemptRestartOfPreviousStages()
        {
            if (PreviousStage == null) return;

            foreach (var s in PreviousStage.Stages.OrderBy(s => s.ExpectedFinishTime))
            {
                if (s.IsBlocked)
                    s.Unblock();

                if (s.IsFinishedProcessing)
                    if (s.PassItemToOutboundQueue())
                        if (s.TakeItemFromInboundQueue())
                            s.StartProcessingItem();
            }
        }

    }
}