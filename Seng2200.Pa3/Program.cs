﻿using System;

namespace Seng2200.Pa3
{
    class Program
    {
        static void Main(string[] args)
        {
            Simulation sim = new Simulation(10, 10, 2);
            sim.Run();
        }
    }
}
