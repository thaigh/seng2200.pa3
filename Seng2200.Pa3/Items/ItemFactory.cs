﻿using System;
namespace Seng2200.Pa3.Items
{
    public class ItemCreatedEventArgs : EventArgs
    {
        public Item CreatedItem { get; set; }
    }

    public class ItemFactory
    {
        public event EventHandler<ItemCreatedEventArgs> CreatedItem;

        public IProcessingItem Create()
        {
            Item itm = new Item();

            CreatedItem?.Invoke(this, new ItemCreatedEventArgs { CreatedItem = itm });

            return itm;
        }
    }
}
