﻿namespace Seng2200.Pa3.Items
{
    public enum EventType
    {
        EnterProductionStage,
        ExitProductionStage,
        EnterStorageQueue,
        ExitStorageQueue
    }
}