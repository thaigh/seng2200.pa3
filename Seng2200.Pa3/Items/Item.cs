﻿using System;
using System.Collections.Generic;
namespace Seng2200.Pa3.Items
{
    public class Item : IProcessingItem
    {
        public readonly string UniqueId;

        private double _timeWhenGoingIntoIdle;
        public bool IsIdle { get; private set; }

        private double _totalTimeSpentIdling = 0;
        public double TotalTimeSpentIdling { get { return _totalTimeSpentIdling; } }

        public double TimeOfCreation { get; set; }
        public double FinishTime { get; private set; }
        public double ThroughputTime { get { return FinishTime - TimeOfCreation; } }

        // Singleton Pattern ID Generation
        private static int NextId = 0;
        private static int GetId() { return NextId++; }

        public Item()
        {
            UniqueId = string.Format("{0}", GetId());
        }

        public void GoIdle(double currentTime)
        {
            if (!IsIdle)
            {
                _timeWhenGoingIntoIdle = currentTime;
                IsIdle = true;
            }
        }

        public void GoOutOfIdle(double currentTime)
        {
            if (IsIdle)
            {

                double timeSpentIdling = currentTime - _timeWhenGoingIntoIdle;
                _totalTimeSpentIdling += timeSpentIdling;
                IsIdle = false;
            }
        }

        public void FinishItemOnProductionLine(double currentTime)
        {
            FinishTime = currentTime;
        }

        public override string ToString()
        {
            return string.Format("[Item: {0}, Created At: {1}, IsIdle: {2}]", UniqueId, TimeOfCreation, IsIdle);
        }

    }
}
