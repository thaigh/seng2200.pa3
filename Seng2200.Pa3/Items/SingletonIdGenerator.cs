﻿namespace Seng2200.Pa3.Items
{

    public sealed class SingletonIdGenerator
    {
        private static int NextId = 0;

        private static readonly SingletonIdGenerator _instance = new SingletonIdGenerator();
        public static SingletonIdGenerator Instance { get { return _instance; } }

        private SingletonIdGenerator() { }

        public int GetId()
        {
            return NextId++;
        }

    }
}