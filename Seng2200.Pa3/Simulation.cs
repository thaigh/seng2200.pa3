﻿using System;
using Seng2200.Pa3.Stages;
using System.Collections.Generic;
using Seng2200.Pa3.Storage;
using Seng2200.Pa3.Items;
using System.Linq;
namespace Seng2200.Pa3
{
    public class Simulation
    {
        private int _mean;
        private int _range;
        private int _queueMax;

        private const double SimulationDuration = 1000;
        private double _currentTime = 0;

        PriorityQueue<SimulationEvent> _pq = new PriorityQueue<SimulationEvent>(new EventComparer());

        public Simulation(int mean, int range, int queueMax)
        {
            _mean = mean;
            _range = range;
            _queueMax = queueMax;
        }

        public void Run()
        {
            List<MasterStage> stages = SetupSimulation();
            _currentTime = 0;

            SimulationEvent nextEvent = null;

            while (_currentTime < SimulationDuration)
            {
                if (nextEvent == null)
                {
                    // First pass through simulation. setup first stages
                    foreach (var stage in stages.First().Stages)
                    {
                        if (stage.IsStarved)
                            stage.Unstarve();
                        
                        stage.TakeItemFromInboundQueue();
                        stage.StartProcessingItem();
                    }
                }
                else
                {
                    // Handle event
                    SubStage stage = nextEvent.Stage;

                    if (stage.IsProcessing)
                        stage.FinishItem();

                    if (stage.IsBlocked)
                        stage.Unblock();

                    // If we can't pass it on, then we block
                    if (stage.PassItemToOutboundQueue())
                    {
                        // If we can't take an item, then we starve
                        if (stage.TakeItemFromInboundQueue())
                        {
                            stage.StartProcessingItem();
                        }
                    }
                }

                nextEvent = _pq.Dequeue();
                _currentTime = nextEvent.FinishProcessingTime;

                Console.WriteLine("=== Updating simulation clock to {0} ===", _currentTime);
            }

        }

        private List<MasterStage> SetupSimulation()
        {
            // Setup factory
            ItemFactory factory = new ItemFactory();

            // Setup Stations
            MasterStage s0 = new MasterStage("S0", new List<SubStage> {
                new SubStage(_mean, _range, 1) { Name = "S0A" },
                new SubStage(_mean, _range, 2) { Name = "S0B" }
            });

            MasterStage s1 = new MasterStage("S1", new List<SubStage> {
                new SubStage(_mean, _range, 1) { Name = "S1A" }
            });

            MasterStage s2 = new MasterStage("S2", new List<SubStage> {
                new SubStage(_mean, _range, 2) { Name = "S2A" },
                new SubStage(_mean, _range, 2) { Name = "S2B" }
            });

            // Setup Queues
            InfiniteInboundStorage iis = new InfiniteInboundStorage(s0, factory) { Name = "Infinite Inbound Q-0" };
            InterStageStorage q1 = new InterStageStorage(_queueMax, s1, s0) { Name = "Inter-Storage Q01" };
            InterStageStorage q2 = new InterStageStorage(_queueMax, s2, s1) { Name = "Inter-Storage Q12" };
            InfiniteOutboundStorage ios = new InfiniteOutboundStorage(s2) { Name = "Infinite Outbound Q1-" };

            // Setup Event Handlers
            factory.CreatedItem += AddCreationTimeToItem;
            ios.FinishedItemProcessing += UpdateItemFinishedTime;
            s0.StartProcessingEvent += AddSimulationEventToQueue;
            s1.StartProcessingEvent += AddSimulationEventToQueue;
            s2.StartProcessingEvent += AddSimulationEventToQueue;

            // Link queues to stages
            s0.AddStorage(iis, StorageType.Inbound);
            s0.AddStorage(q1, StorageType.Outbound);

            s1.AddStorage(q1, StorageType.Inbound);
            s1.AddStorage(q2, StorageType.Outbound);

            s2.AddStorage(q2, StorageType.Inbound);
            s2.AddStorage(ios, StorageType.Outbound);

            return new List<MasterStage> {
                s0, s1, s2
            };
        }

        // Event Handlers

        private void AddCreationTimeToItem(object sender, ItemCreatedEventArgs e)
        {
            e.CreatedItem.TimeOfCreation = _currentTime;

            Console.WriteLine("Creating new item {0} at time {1}", e.CreatedItem.UniqueId, _currentTime);
        }

        private void UpdateItemFinishedTime(object sender, FinishedItemEventArgs e)
        {
            e.FinishedItem.FinishItemOnProductionLine(_currentTime);

            Console.WriteLine("Finishing item {0} at time {1}", e.FinishedItem.UniqueId, _currentTime);
        }

        private void AddSimulationEventToQueue(object sender, StartProcessingEventArgs e)
        {
            double expectedFinishTime = _currentTime + e.RequiredProcessingTime;

            SimulationEvent evnt = new SimulationEvent()
            {
                Stage = e.Stage,
                FinishProcessingTime = expectedFinishTime
            };

            _pq.Enqueue(evnt);

            evnt.Stage.ExpectedFinishTime = expectedFinishTime;

            Console.WriteLine("Stage {0}: Starting processing of item. Will finish at {1}", evnt.Stage.Name, expectedFinishTime);
        }
    }
}
