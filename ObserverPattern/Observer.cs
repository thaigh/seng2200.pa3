﻿using System;
namespace ObserverPattern
{
    public abstract class Observer
    {
        public abstract void Update(ObservableMessage message);
        protected IObservable Subject { get; set; }
    }
}
