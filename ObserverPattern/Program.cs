﻿using System;

namespace ObserverPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            DecimalSubject subject = new DecimalSubject();
            BinaryObserver bin = new BinaryObserver();
            OctalObserver oct = new OctalObserver();
            DecimalObserver dec = new DecimalObserver();
            HexObserver hex = new HexObserver();

            subject.AttachObserver(bin);
            subject.AttachObserver(oct);
            subject.AttachObserver(dec);
            subject.AttachObserver(hex);

            subject.SetValue(10);
        }
    }
}
