﻿using System;
namespace ObserverPattern
{
    public interface IObservable
    {
        void AttachObserver(Observer obs);
        void NotifyAllObservers();

    }
}
