﻿using System;
using System.Collections.Generic;
namespace ObserverPattern
{
    public class BinaryObserver : Observer
    {
        public override void Update(ObservableMessage message)
        {
            // Unwrap message
            if (message is ValueChangeMessage)
            {
                ValueChangeMessage m = (ValueChangeMessage)message;

                Console.WriteLine("Binary Observer: Value is {0}", Convert.ToString(m.Value, 2));
            }
        }
    }

    public class OctalObserver : Observer
    {
        public override void Update(ObservableMessage message)
        {
            // Unwrap message
            if (message is ValueChangeMessage)
            {
                ValueChangeMessage m = (ValueChangeMessage)message;

                Console.WriteLine("Octal Observer: Value is {0}", Convert.ToString(m.Value, 8));
            }
        }
    }

    public class DecimalObserver : Observer
    {
        public override void Update(ObservableMessage message)
        {
            // Unwrap message
            if (message is ValueChangeMessage)
            {
                ValueChangeMessage m = (ValueChangeMessage)message;

                Console.WriteLine("Decimal Observer: Value is {0}", Convert.ToString(m.Value, 10));
            }
        }
    }

    public class HexObserver : Observer
    {
        public override void Update(ObservableMessage message)
        {
            // Unwrap message
            if (message is ValueChangeMessage)
            {
                ValueChangeMessage m = (ValueChangeMessage)message;

                Console.WriteLine("Hexadecimal Observer: Value is {0}", Convert.ToString(m.Value, 16));
            }
        }
    }
}
