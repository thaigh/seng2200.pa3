﻿using System;
namespace ObserverPattern
{
    public class ValueChangeMessage : ObservableMessage
    {
        public int Value { get; private set; }

        public ValueChangeMessage(int val)
        {
            Value = val;
        }
    }
}
