﻿using System;
using System.Collections.Generic;
namespace ObserverPattern
{
    public class DecimalSubject : IObservable
    {
        private List<Observer> _observers = new List<Observer>();
        private int _val;

        public void AttachObserver(Observer obs)
        {
            _observers.Add(obs);
        }

        public void SetValue(int val)
        {
            _val = val;
            NotifyAllObservers();
        }

        public void NotifyAllObservers()
        {
            ObservableMessage message = new ValueChangeMessage(_val);

            foreach (var obs in _observers)
            {
                obs.Update(message);
            }
        }
    }
}
