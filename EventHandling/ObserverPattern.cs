﻿using System;
namespace EventHandling
{
    public interface IObservable {
        void Attach(IObserver obs);
        void NotifyObservers();
    }

    public interface IObserver {
        void Update(ObservableMessage message);
    }

    public abstract class ObservableMessage { }

}
