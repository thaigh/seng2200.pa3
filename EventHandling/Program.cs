﻿using System;

namespace EventHandling
{
    class Program : IObserver
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            p.Run();
        }

        public void Run()
        {
            NotificationCenter nc = new NotificationCenter();

            //nc.Attach(this);
            nc.SendUpdate += Nc_SendUpdate;

            nc.Simulate();
        }

        private void Nc_SendUpdate(object sender, EventArgs e)
        {
            CustomEventArgs cn = (CustomEventArgs)e;

            Console.WriteLine("Received message from something I was observing. The message says");
            Console.WriteLine("\t {0}", cn.MessageText);
        }

        public void Update(ObservableMessage message)
        {
            if (message is CustomNotification)
            {
                CustomNotification cn = (CustomNotification)message;

                Console.WriteLine("Received message from something I was observing. The message says");
                Console.WriteLine("\t {0}", cn.MessageText);
            }
        }
    }
}
