﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace EventHandling
{
    public class CustomNotification : ObservableMessage {
        public string MessageText { get; set; }
    }

    public class CustomEventArgs : EventArgs {
        public string MessageText { get; set; }
    }

    public class NotificationCenter : IObservable
    {
        public event EventHandler SendUpdate;

        private List<IObserver> _observers = new List<IObserver>();

        public void Attach(IObserver obs)
        {
            _observers.Add(obs);
        }

        // Notification Center Specific Code

        public void Simulate()
        {
            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(1000);
                NotifyObservers();
            }
        }

        public void NotifyObservers()
        {
            //CustomNotification note = new CustomNotification() { MessageText = "Hello" };
            //foreach (var obs in _observers)
            //{
            //    obs.Update(note);
            //}

            CustomEventArgs args = new CustomEventArgs() { MessageText = "Custom Event" };
            SendUpdate?.Invoke(this, args);
        }
    }
}
